const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const app = express();
const appRouter = require('./router/appRouter');

dotenv.config();
app.use(express.json());
app.use(cors());
app.use(appRouter);

mongoose.connect(process.env.DB_CONNECT_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    app.listen(5000);
    console.log('Node-Conn-Established');
  })
  .catch((err) => {
    console.log(err);
  });
