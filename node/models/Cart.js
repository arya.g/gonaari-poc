const mongoose = require('mongoose');

const CartSchema = new mongoose.Schema({
  item: {
    type: String
  },
  item_title: {
    type: String
  },
  item_price: {
    type: Number
  },
  base_currency: {
    type: String
  },
  img_url: {
    type: String
  },
  qty: {
    type: Number
  }
});

module.exports = mongoose.model('Cart', CartSchema);
