const { Router } = require('express');
const appRouter = Router();
const Cart = require('../models/Cart.js');

appRouter.get('/cart', async (req, res) => {
  try {
    const cart = await Cart.find();
    res.status(200).send(cart);
  } catch (error) {
    res.status(400).send(error);
  }
});

appRouter.post('/cart', async (req, res) => {
  try {
    const newCart = new Cart(req.body);
    
    await newCart.save().then(() => {
      res.status(201).send({ message: 'success' });
    })
  } catch (error) {
    console.log(error)
    res.status(400).send(error);
  }
});

appRouter.patch('/cart/add/:id/:qty', async (req, res) => {
  try {
    await Cart.updateOne(
      { _id: req.params.id },
      {
        $set: {
          qty: req.params.qty
        }
      }
    ).then(() => {
      res.status(201).send({ message: 'success' });
    });
  } catch (error) {
    res.status(400).send(error);
  }
});

appRouter.get('/user', async (req, res) => {
  try {
    res.status(200).send([
      {
        id: 0,
        name: 'Arya'
      },
      {
        id: 1,
        name: 'Virat'
      },
      {
        id: 2,
        name: 'Smith'
      }
    ]);
  } catch (error) {
    res.status(400).send(error);
  }
});

appRouter.get('/login', async (req, res) => {
  try {
    res.status(200).send({
      message: 'success',
      id: 1,
      name: 'Test User'
    });
  } catch (error) {
    res.status(400).send(error);
  }
});

module.exports = appRouter;
