const MongoClient = require('mongodb').MongoClient;

async function seedDB() {
  const uri =
    'mongodb+srv://additop78:myaccess_123***@clustersalaam.rvt48.azure.mongodb.net/artists-app?retryWrites=true&w=majority';

  const client = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

  try {
    await client.connect();
    console.log('client-conn-established');
    const db = client.db('artists-app');
    const existingCart = db.collection("Cart").find().toArray()

    if (existingCart.length) {
      existingCart.drop()
      console.log("existing cart dropped")
    }

    const sampleCart = [
      {
        id: '0',
        item: 'Sweater',
        item_title:
          'Highly effective winter apparel which works even in Alaska',
        item_price: 250,
        base_currency: 'inr',
        img_url: 'https://picsum.photos/seed/5/200/300',
        qty: 33
      },
      {
        id: '1',
        item: 'Grooming Kit',
        item_title: 'Glam up like never before on the red carpet',
        item_price: 500,
        base_currency: 'inr',
        img_url: 'https://picsum.photos/seed/5/200/300',
        qty: 2
      },
      {
        id: '3',
        item: 'Shampoo',
        item_title:
          "Long and shiny hair which you've been dreaming for ever so long",
        item_price: 350,
        base_currency: 'inr',
        img_url: 'https://picsum.photos/seed/5/200/300',
        qty: 2
      }
    ];

    await db.collection("Cart").insertMany(sampleCart).then(() => {
      console.log('sample cart inserted');
      client.close();
    });
  } catch (error) {
    console.log(error);
    client.close();
  }
}

seedDB();
