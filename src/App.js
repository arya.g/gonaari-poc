import React from 'react'
import { useRoutes, BrowserRouter } from 'react-router-dom'
import Routes from './router/router'
import AuthContext from './auth/AuthContext'

const RouterView = () => {
  const routeConfig = useRoutes(Routes())
  return routeConfig
}

function App() {
  return (
    <div>
      <BrowserRouter>
        <AuthContext>
          <RouterView />
        </AuthContext>
      </BrowserRouter>
    </div>
  )
}

export default App
