// wrapper component to protect routes
// uses the auth token stored in cookies to authenticate & set user
// TODO:- update logic as necessary

import { useContext, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import { UserContext } from './AuthContext'
import { useLoginUserQuery } from '../services/userService'

axios.defaults.withCredentials = true

function AuthProvider({ children }) {
  const [user, setUser] = useContext(UserContext)
  const { data, error } = useLoginUserQuery()
  const navigate = useNavigate()

  useEffect(() => {
    if (error) {
      navigate('/')
    }

    if (data) {
      if (!data.id) {
        navigate('/')
        return
      }

      setUser(data)
    }
  }, [data, user])

  return user && user.id ? children : <></>
}

export default AuthProvider
