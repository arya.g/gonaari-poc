import axios from 'axios'
import { getBaseUrl } from '../utils/getBaseUrl'

const baseURL = getBaseUrl()

/**
 *
 * @param {config} Object
 * @param {config.endpoint} String
 * @param {config.method} String
 * @param {config.data} Object
 * @param {config.params} Object
 * @param {config.headers} Object
 * @param {config.queryString} String
 */

// Todo:- add config for PUT request too if necessary
//* example:- const res = await useAxios({config})

const useAxios = async (config) => {
  const apiURL = `${baseURL}/${config.endpoint}`

  if (config && config.queryString) {
    apiURL += config.queryString
  }

  switch (config.method) {
    // GET
    case 'GET':
      await axios
        .get(apiURL, {
          params: config.params,
          headers: config.headers,
        })
        .then(async (res) => {
          if (res.status === 200) {
            return res
          }

          return console.log(
            `Get Request to ${config.endpoint} failed with Error Code ${res.status}`,
          )
        })
        .catch((err) => console.log('Unspecified error occured:- ', err))
      break

    // POST
    case 'POST':
      await axios
        .post(apiURL, config.data, {
          params: config.params,
          headers: config.headers,
        })
        .then(async (res) => {
          if (res.status === 201) {
            return console.log('Success')
          }

          return console.log(
            `Post Request to ${config.endpoint} Failed with Error Code ${res.status}`,
          )
        })
        .catch((err) => console.log('Unspecified error occured:- ', err))
      break

    // PATCH
    case 'PATCH':
      await axios
        .patch(apiURL, config.data, {
          params: config.params,
          headers: config.headers,
        })
        .then(async (res) => {
          if (res.status === 200 || res.status === 201 || res.status === 204) {
            return console.log('Success')
          }

          return console.log(
            `Patch Request to ${config.endpoint} Failed with Error Code ${res.status}`,
          )
        })
        .catch((err) => console.log('Unspecified error occured:- ', err))
      break

    // DELETE
    case 'DELETE':
      await axios
        .delete(apiURL, config.data, {
          params: config.params,
          headers: config.headers,
        })
        .then(async (res) => {
          if (res.status === 200) {
            return console.log('Success')
          }

          return console.log(
            `Delete Request to ${config.endpoint} Failed with Error Code ${res.status}`,
          )
        })
        .catch((err) => console.log('Unspecified error occured:- ', err))
      break

    default:
      break
  }
}

export default useAxios
