import React from 'react'

function ActionWidget() {
  return (
    <div className="m-action-widget">
      <div
        className="m-action-widget__action-icon-wrapper"
        title="Add to Wishlist"
      >
        <i className="m-action-widget__action-icon m-action-widget__action-icon--wishlist"></i>
      </div>

      <div className="m-action-widget__action-icon-wrapper" title="Add to Cart">
        <i className="m-action-widget__action-icon m-action-widget__action-icon--cart"></i>
      </div>

      <div className="m-action-widget__action-icon-wrapper" title="Quick view">
        <i className="m-action-widget__action-icon m-action-widget__action-icon--view"></i>
      </div>
    </div>
  )
}

export default ActionWidget
