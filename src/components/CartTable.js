import React from 'react'
import QuantityWidget from './QuantityWidget'

function CartTable({ cart, setItemQuantity }) {
  const itemTotal = (item) => item.qty * item.item_price

  return (
    <table className="m-cart-table l-site-container">
      <thead className="m-cart-table__thead">
        <tr>
          <th></th>
          <th className="m-cart-table__th" colSpan={2}>
            Product
          </th>
          <th className="m-cart-table__th">MRP</th>
          <th className="m-cart-table__th">Price</th>
          <th className="m-cart-table__th">Quantity</th>
          <th className="m-cart-table__th">Total</th>
        </tr>
      </thead>

      <tbody className="m-cart-table__tbody">
        {cart &&
          cart.map((item) => (
            <tr key={item._id}>
              <td className="m-cart-table__close-icon"></td>
              <td className="m-cart-table__td">
                <img
                  className="m-cart-table__product-image"
                  src="https://picsum.photos/90/90"
                  alt="cart-item"
                />
              </td>
              <td className="m-cart-table__td m-cart-table__product-title">
                {item.item_title}
              </td>
              <td className="m-cart-table__td m-cart-table__product-mrp">
                {item.item_price}
              </td>
              <td className="m-cart-table__td m-cart-table__product-price">
                {item.item_price}
              </td>
              <td className="m-cart-table__td">
                <div className="m-cart-table__quantity-widget">
                  <QuantityWidget
                    item={item}
                    setItemQuantity={setItemQuantity}
                  />
                </div>
              </td>
              <td className="m-cart-table__td m-cart-table__product-total">
                {itemTotal(item)}
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  )
}

export default CartTable
