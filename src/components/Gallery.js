import React, { useState } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import { EffectFade } from 'swiper'
import 'swiper/css'
import 'swiper/css/effect-fade'

function Gallery() {
  const [swiperInstance, setSwiperInstance] = useState()

  const moveSlide = (action) =>
    action === 'previous'
      ? swiperInstance.slidePrev()
      : swiperInstance.slideNext()

  return (
    <div className="m-gallery">
      <Swiper
        spaceBetween={0}
        slidesPerView={1}
        loop
        speed={1500}
        effect="fade"
        onSlideChange={() => {}}
        onSwiper={(swiper) => {
          setSwiperInstance(swiper)
        }}
        id="swiper"
        modules={[EffectFade]}
        className="l-site-container"
      >
        {[...Array(5)].map((e, i) => (
          <SwiperSlide key={i}>
            <img
              className="m-gallery__pic"
              alt={`pic_${i}`}
              src={`https://picsum.photos/seed/${i}/200/300`}
            />
          </SwiperSlide>
        ))}
        <button
          className="m-gallery__arrow-btn m-gallery__arrow-btn--left"
          onClick={() => {
            moveSlide('previous')
          }}
        ></button>
        <button
          className="m-gallery__arrow-btn m-gallery__arrow-btn--right"
          onClick={() => {
            moveSlide('next')
          }}
        ></button>
      </Swiper>
    </div>
  )
}

export default Gallery
