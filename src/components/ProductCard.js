import React from 'react'
import ActionWidget from './ActionWidget'

function ProductCard({ seed, showActionWidget }) {
  // only show action widget for screens greater than iPad portrait
  const renderActionWidget = () => {
    const screenWidth = window.innerWidth
    return !(screenWidth <= 768 || !showActionWidget)
  }

  return (
    <div className="m-product-card">
      <img
        className="m-product-card__product-image"
        src={`https://picsum.photos/seed/${seed}/200/300`}
      ></img>

      <h5 className="m-product-card__product-title">
        Purplle VIVIFY Max Volume + Length Mascara | Volumizing | Lenghtening |
        Long Lasting | Smudgeproof (9.5 Gm)
      </h5>     

      <div className="m-product-card__price-wrapper">
        <span className="m-product-card__mrp">MRP</span>
        <span className="m-product-card__price">510</span>
      </div>

      {renderActionWidget() ? <ActionWidget /> : <></>}
    </div>
  )
}

export default ProductCard
