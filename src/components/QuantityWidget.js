import React from 'react'

function QuantityWidget({ item, setItemQuantity }) {
  const changeQuantity = (action) => {
    if (action === 'subtract') {
      if (item.qty === 1) {
        return
      }
      const newQty = item.qty - 1
      setItemQuantity(item._id, newQty)
    }

    if (action === 'add') {
      const newQty = item.qty + 1
      setItemQuantity(item._id, newQty)
    }
  }

  return (
    <div className="m-quantity-widget">
      <div
        className="m-quantity-widget__subtract"
        onClick={() => {
          changeQuantity('subtract')
        }}
      >
        -
      </div>
      <div className="m-quantity-widget__quantity">{item.qty}</div>
      <div
        className="m-quantity-widget__add"
        onClick={() => {
          changeQuantity('add')
        }}
      >
        +
      </div>
    </div>
  )
}

export default QuantityWidget
