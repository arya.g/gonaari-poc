import React from 'react'

function SearchBar({ isSearchBarOpen }) {
  return (
    <div className="m-searchbar l-site-container">
      <div className="m-searchbar__content-wrapper">
        <label htmlFor="searchbar" className="m-searchbar__ip-label">
          What are you looking for?
        </label>
        <input
          className="m-searchbar__ip-bar"
          placeholder="Search"
          id="searchbar"
        />
        <i
          className="m-icon m-icon--close"
          onClick={() => isSearchBarOpen(false)}
        ></i>
      </div>
    </div>
  )
}

export default SearchBar
