import React from 'react'

function SideBar({ isSideBarOpen }) {
  const toggleOpen = (id) => {
    const topEl = document.getElementById(id)
    return topEl.classList.contains('is-open')
      ? topEl.classList.remove('is-open')
      : topEl.classList.add('is-open')
  }

  return (
    <aside className="m-sidebar">
      <ul className="m-sidebar__wrapper">
        <div className="m-sidebar__close">
          <span>Close Menu</span>{' '}
          <i
            className="m-sidebar__close-icon"
            onClick={() => {
              isSideBarOpen(false)
            }}
          ></i>
        </div>
        <li className="m-sidebar__main-items-list">
          <span className="m-sidebar__main-item-title" id="id-categories">
            <span>Categories</span>
            <i
              className="m-sidebar__indication-icon"
              onClick={() => {
                toggleOpen('id-categories')
              }}
            ></i>
          </span>
          <ul className="m-sidebar__items">
            <li className="m-sidebar__items-list">
              <span className="m-sidebar__item-title" id="id-makup">
                <span>Makeup</span>
                <i
                  className="m-sidebar__indication-icon"
                  onClick={() => {
                    toggleOpen('id-makup')
                  }}
                ></i>
              </span>
              <ul className="m-sidebar__sub-items-list">
                <li className="m-sidebar__sub-item">Face Makeup</li>
                <li className="m-sidebar__sub-item">Lip Makeup</li>
                <li className="m-sidebar__sub-item">Eye Makeup</li>
                <li className="m-sidebar__sub-item">Brushes & Tools</li>
              </ul>
            </li>
          </ul>
          <ul className="m-sidebar__items">
            <li className="m-sidebar__items-list">
              <span className="m-sidebar__item-title" id="id-clothing">
                <span>Clothing</span>
                <i
                  className="m-sidebar__indication-icon"
                  onClick={() => {
                    toggleOpen('id-clothing')
                  }}
                ></i>
              </span>
              <ul className="m-sidebar__sub-items-list">
                <li className="m-sidebar__sub-item">Sweater</li>
                <li className="m-sidebar__sub-item">Hoodie</li>
                <li className="m-sidebar__sub-item">Tee</li>
                <li className="m-sidebar__sub-item">Pants</li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </aside>
  )
}

export default SideBar
