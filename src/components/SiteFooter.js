import React from 'react'

function SiteFooter() {
  const toggleOpen = (id) => {
    const el = document.getElementById(id)

    if (el.classList.contains('is-open')) {
      el.classList.remove('is-open')
    } else {
      el.classList.add('is-open')
    }
  }

  return (
    <footer className="m-site-footer">
      <div className="m-site-footer__content-wrapper l-site-container">
        <ul className="m-site-footer__expandable-column">
          <li className="m-site-footer__expandable-column-title">
            <span
              className="m-site-footer__expandable-column-title-text"
              id="quick-shop"
            >
              <span>Quick Shop</span>
              <i
                className="m-site-footer__collapse-icon"
                onClick={() => toggleOpen('quick-shop')}
              ></i>
            </span>
            <ul className="m-site-footer__expandable-column-sub-list">
              <li className="m-site-footer__expandable-column-item">Makeup</li>
              <li className="m-site-footer__expandable-column-item">
                Hair Care
              </li>
              <li className="m-site-footer__expandable-column-item">
                Skin Care
              </li>
              <li className="m-site-footer__expandable-column-item">
                Fragrance
              </li>
              <li className="m-site-footer__expandable-column-item">
                Personal Care
              </li>
            </ul>
          </li>
        </ul>
        <div className="m-site-footer__non-expandable-column">
          <span className="m-site-footer__non-expandable-column-title-text">
            Contact Us
          </span>
          <p className="m-site-footer__non-expandable-column-text">
            Embassy 247, B-Wing, 10th floor, Lal Bahadur Shastri Rd, Gandhi
            Nagar, Vikhroli West, Mumbai, Maharashtra 400079
          </p>
        </div>
      </div>
    </footer>
  )
}

export default SiteFooter
