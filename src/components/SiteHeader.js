import { useNavigate } from 'react-router-dom'
import React from 'react'
import { useGetCartQuery } from '../services/cartService'

function SiteHeader({ isSearchBarOpen, isSideBarOpen }) {
  const { data, error, isSuccess, isLoading } = useGetCartQuery()
  const navigate = useNavigate()

  return (
    <header>
      <div className="m-header l-site-container">
        <div className="m-header__icons">
          <i
            className="m-header__icon m-header__icon--menu"
            onClick={() => isSideBarOpen(true)}
          ></i>
          <i
            className="m-header__icon m-header__icon--search"
            onClick={() => isSearchBarOpen(true)}
          ></i>
        </div>
        <img
          src="https://cdn.shopify.com/s/files/1/0636/6619/2605/files/gonari_logo_120_X_30_ba3c7c10-4a38-429d-8a9e-b3f45c456736_130x.png?v=1649941319"
          className="m-header__logo"
          alt="GoNaari"
          onClick={() => navigate('/')}
        />
        <div className="m-header__icons-wrapper">
          <i className="m-header__icon m-header__icon--user"></i>
          <div className="m-header__cart-icon-wrapper">
            <i
              className="m-header__icon m-header__icon--cart"
              onClick={() => navigate('/cart')}
            ></i>
            {data && isSuccess ? (
              <span className="m-header__cart-number"> {data.length} </span>
            ) : (
              <></>
            )}
          </div>
        </div>
      </div>
    </header>
  )
}

export default SiteHeader
