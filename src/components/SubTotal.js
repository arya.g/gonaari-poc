import React from 'react'

function SubTotal({ subTotal, cart }) {
  return (
    <div className="m-subtotal">
      <div className="m-subtotal__value-wrapper">
        <p className="m-subtotal__text">Subtotal</p>
        <span className="m-subtotal__value">{subTotal}</span>
      </div>

      <p className="m-subtotal__tax-info">
        Shipping & taxes calculated at checkout
      </p>

      <div className="m-subtotal__coupon-code-wrapper">
        <input
          className="m-subtotal__code-ip"
          placeholder="Enter coupon code"
        />
        <button className="m-subtotal__apply-btn">Apply</button>
      </div>

      <button className="m-subtotal__checkout-btn">Proceed To Checkout</button>
    </div>
  )
}

export default SubTotal
