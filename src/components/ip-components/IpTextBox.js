import { useEffect } from 'react'

function IpTextBox(props) {
  // received-props-ip-textbox
  const {
    htmlId,
    disabled,
    placeholder,
    autoComplete,
    isTypePassword,
    errorText,
    labelText,
    valueEmitter,
  } = props

  const bindId = () => {
    let id = ''

    if (!htmlId) {
      id = Math.floor(Math.random() * 1000)

      if (labelText) {
        id += labelText.split(' ').join('-')
      }
    } else {
      id = htmlId
    }

    return id.toString()
  }

  useEffect(() => {
    bindId()
  }, [])

  return (
    <div className="m-ip-textbox">
      {labelText ? (
        <label className="m-ip-textbox__label">{labelText}</label>
      ) : (
        <></>
      )}

      <input
        className="m-ip-textbox__ip"
        id={bindId()}
        disabled={disabled}
        placeholder={placeholder}
        autoComplete={autoComplete}
        type={isTypePassword ? 'password' : 'text'}
        onChange={(e) => {
          valueEmitter(e.target.value)
        }}
      />

      {errorText ? (
        <span className="m-ip-textbox__error">{errorText}</span>
      ) : (
        <></>
      )}
    </div>
  )
}

export default IpTextBox
