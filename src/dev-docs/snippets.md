# Useful snippets for quick reference

## Update one property of an object in a state array

```js
import {useState} from 'react';

const App = () => {
  const initialState = [
    {id: 1, country: 'Austria'},
    {id: 2, country: 'Belgium'},
    {id: 3, country: 'Canada'},
  ];

  const [data, setData] = useState(initialState);

  const updateState = () => {
    const newState = data.map(obj => {
      // 👇️ if id equals 2, update country property
      if (obj.id === 2) {
        return {...obj, country: 'Denmark'};
      }

      // 👇️ otherwise return object as is
      return obj;
    });

    setData(newState);
  };
```
Reference:- See the `setItemQuantity` method in `pages/Cart.js`

## Prevent content jumping simply with CSS

```CSS
.list {
  height: 0;
  opacity: 0;
  pointer-events: none;
  overflow: hidden;
  transition: height 0.66s ease-out;
}
.list.opened {
  height: fit-content;
  opacity: 1;
  pointer-events: all;
  transition: all 0.5s;
}
```
[Reference Here](https://css-tricks.com/content-jumping-avoid/)
