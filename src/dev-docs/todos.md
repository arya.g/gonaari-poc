# Developer Todos

## 1. Improvise container layout for all screens
### Priority - Substantial
Reference - `css/_layouts.scss`

## 2. Improvise slider component to render children as slides
### Priority - Moderate
Reference - `components/Gallery.js`