# What are page layouts?

Page layouts are standalone components which contain globally-present elements.
For example, almost every site has a header and a footer. The header and footer are present in every page.
If you just import and place the header and footer components in every page, you will also need to duplicate their logic.
For example, a `showSearchBar` function in header needs to be duplicated in all pages.
We know that duplication of code is always to be avoided. Therefore, what we can do is, create a layout component
named `DefaultLayout.js` which contains the header and footer. But this component will also accept a special prop called
`children` which is nothing but page content.

In VueJS, we can achieve this using named slots. In React however, we can mimic that functionality as follows.

Suppose we have two pages, namely Home page and About page. What we can do while using layout is, we can just pass
page-specific content to the DefaultLayout as follows:-

- DefaultLayout.js

```js
const DefaultLayout = ({ children }) => {
  return (
    <div>
      <Header />
      {children}
      <Footer />
    <div>
  )
}
```

- Home.js

```js
const Home = () => {
  return (
    <DefaultLayout>
      <h1>This is HOME page</h1>
    </DefaultLayout>
  );
};
```

- About.js

```js
const About = () => {
  return (
    <DefaultLayout>
      <h1>This is ABOUT page</h1>
    </DefaultLayout>
  );
};
```

If you see above, both our home and About pages will ship with header and footer, and their functionality, without the need for verbose duplication.

For reference of implementation in this project, see usage of `layouts/DefaultLayout.js` in `pages/Home.js` and `pages/Cart.js`.
