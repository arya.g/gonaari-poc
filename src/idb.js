import Dexie from 'dexie'

class cartDB extends Dexie {
  items

  constructor() {
    super('cartDB')

    this.version(1).stores({
      items: '_id, item, item_title, item_price, base_currency, img_url, qty'
    })
  }
}

export const db = new cartDB()
