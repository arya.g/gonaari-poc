import React, { useState, useEffect } from 'react'
import SiteHeader from '../components/SiteHeader'
import SiteFooter from '../components/SiteFooter'
import SearchBar from '../components/SearchBar'
import SideBar from '../components/SideBar'
import { useGetUsersQuery } from '../services/userService'

function DefaultLayout({ children }) {
  const [revealSearchBar, setRevealSearchBar] = useState(false)
  const [revealSideBar, setRevealSideBar] = useState(false)
  const [users, setUsers] = useState([])
  const { data } = useGetUsersQuery()

  useEffect(() => {
    if (data) {
      setUsers(data)
    }
  }, [])

  const isSearchBarOpen = (isOpen) => {
    setRevealSearchBar(isOpen)
  }

  const isSideBarOpen = (isOpen) => {
    setRevealSideBar(isOpen)
  }

  return (
    <div>
      {revealSearchBar ? (
        <SearchBar isSearchBarOpen={isSearchBarOpen} />
      ) : (
        <></>
      )}

      {revealSideBar ? <SideBar isSideBarOpen={isSideBarOpen} /> : <></>}

      <SiteHeader
        className="animate__animated animate__bounceOutRight animate__delay-2s"
        isSearchBarOpen={isSearchBarOpen}
        isSideBarOpen={isSideBarOpen}
      />

      {children}

      <SiteFooter />
    </div>
  )
}

export default DefaultLayout
