import React, { useEffect, useState } from 'react'
import DefaultLayout from '../layouts/DefaultLayout'
import CartTable from '../components/CartTable'
import SubTotal from '../components/SubTotal'
import { useGetCartQuery, useAddToCartMutation } from '../services/cartService'
import { db } from '../idb'
import useNetwork from '../hooks/useNetwork'

function Cart() {
  const connection = useNetwork()

  const [cart, setCart] = useState([])
  const [subTotal, setSubTotal] = useState(0)
  const { data, isSuccess, isFetching } = useGetCartQuery()
  const [updateCart] = useAddToCartMutation()

  const getCart = async () => {
    if (!connection.online) {
      const offlineCart = await db.items.toArray()

      if (offlineCart) {
        setCart(offlineCart)
      }
    } else {
      setCart(data)
    }

    await putCartToIDB()
  }

  const compareCartAndSync = async () => {
    const offlineCart = await db.items.toArray()
    const isIterable = isSuccess && !isFetching && data && data.length

    if (isIterable) {
      Object.keys(offlineCart).forEach((offlineItem) => {
        Object.keys(data).forEach(async (item) => {
          if (
            data[item]._id === offlineCart[offlineItem]._id &&
            offlineCart[offlineItem].qty !== data[item].qty
          ) {
            await updateCart({
              id: data[item]._id,
              qty: offlineCart[offlineItem].qty,
            })
          }
        })
      })

      Object.keys(data).forEach(async (item) => {
        await db.items.put(data[item])
      })
    }
  }

  useEffect(() => {
    if (connection.online) {
      compareCartAndSync()
    }
  }, [connection])

  useEffect(() => {
    async function cartSetter() {
      await getCart()
    }

    cartSetter()
  }, [data])

  const setItemQuantity = async (id, qty) => {
    if (!connection.online) {
      const newCart = cart.map((item) => {
        if (item._id === id) {
          return { ...item, qty }
        }

        return item
      })

      setCart(newCart)
      await putCartToIDB()
    } else {
      await updateCart({ id, qty })
    }
  }

  const putCartToIDB = async () => {
    if (cart && cart.length) {
      cart.forEach(async (item) => {
        await db.items.put(item)
      })
    }
  }

  const getSubTotal = () => {
    const sTotal = cart.reduce(
      (acc, item) => item.qty * item.item_price + acc,
      0,
    )

    return sTotal
  }

  useEffect(() => {
    async function renderCart() {
      if (cart && cart.length) {
        await putCartToIDB()
      }
    }

    renderCart()
  }, [cart])

  useEffect(() => {
    if (cart) {
      setSubTotal(getSubTotal())
    }
  }, [cart])

  return (
    <DefaultLayout>
      <div className="p-cart">
        <h2
          className="p-cart__title"
          style={{ textAlign: 'center', marginBlockStart: '6rem' }}
        >
          Your Cart
        </h2>

        {cart && cart.length && (
          <div>
            <CartTable cart={cart} setItemQuantity={setItemQuantity} />
            <div
              className="p-cart__subtotal-wrapper l-site-container"
              style={{ display: 'flex', justifyContent: 'flex-end' }}
            >
              <SubTotal subTotal={subTotal} cart={cart} />
            </div>
          </div>
        )}
      </div>
    </DefaultLayout>
  )
}

export default Cart
