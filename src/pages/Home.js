import React, { useEffect } from 'react'
import DefaultLayout from '../layouts/DefaultLayout'
import Gallery from '../components/Gallery'
import ProductCard from '../components/ProductCard'
import IpTextBox from '../components/ip-components/IpTextBox'
import { Helmet } from 'react-helmet-async'
import { motion } from 'framer-motion'
import { useSelector } from 'react-redux'

function Home() {
  const { $state } = useSelector((state) => state)

  return (
    <div className="p-home">
      <Helmet>
        <title> Gonaari - Home </title>
        <meta name="description" content="Gonaari Application"></meta>
      </Helmet>
      <DefaultLayout>
        <Gallery className="p-home__gallery l-site-container" />

        <div className="p-home__products l-site-container">
          {[...Array(12)].map((e, i) => (
            <motion.div
              key={i}
              whileHover={{ rotate: 2, marginBlockStart: '2.5rem' }}
              whileTap={{ scale: 1.1 }}
            >
              <ProductCard seed={i} showActionWidget />
            </motion.div>
          ))}
        </div>

        <IpTextBox
          placeholder="Sample placeholder"
          autoComplete="false"
          labelText="Sample label"
          valueEmitter={(val) => {
            console.log(val)
          }}
          isTypePassword
        />
      </DefaultLayout>
    </div>
  )
}

export default Home
