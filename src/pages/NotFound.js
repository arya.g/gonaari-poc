import React from 'react'
import { useNavigate } from 'react-router-dom'
import DefaultLayout from '../layouts/DefaultLayout'

function NotFound() {
  const navigate = useNavigate()

  return (
    <div className="wrapper">
      <DefaultLayout>
        <div className="p-not-found">
          <h1 className="p-not-found__title">Page Not Found</h1>
          <p className="p-not-found__content">
            The page you requested does not exist.
          </p>
          <button
            className="m-btn p-not-found__redirect-btn"
            onClick={() => {
              navigate('/')
            }}
          >
            Continue Shopping
          </button>
        </div>
      </DefaultLayout>
    </div>
  )
}

export default NotFound
