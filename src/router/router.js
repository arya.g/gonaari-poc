import React from 'react'
import AuthProvider from '../auth/AuthProvider'
import Home from '../pages/Home'
import Cart from '../pages/Cart'
import NotFound from '../pages/NotFound'

const Routes = () => [
  {
    path: '/',
    element: <Home />,
  },
  {
    path: '/cart',
    element: (
      <AuthProvider>
        <Cart />
      </AuthProvider>
    ),
  },
  {
    path: '*',
    element: <NotFound />,
  },
]

export default Routes
