import { cartApi } from '../services/cartService'

export const cartSelector = () => cartApi.endpoints.getCart.useQuery()
