import { createSlice } from '@reduxjs/toolkit'

export const $state = createSlice({
  name: '_state',
  initialState: {
    cart: {
        id: "xyz"
    },
  },
  reducers: {
    _setCart(state, action) {
      state.cart = action.payload
    },

  },
})

export const { _setCart } = $state.actions
export default $state.reducer
