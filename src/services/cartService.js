import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { getBaseUrl } from '../utils/getBaseUrl'

const baseURL = getBaseUrl()

export const cartApi = createApi({
  reducerPath: 'cartApi',
  baseQuery: fetchBaseQuery({ baseUrl: baseURL }),
  tagTypes: ['Cart'],
  endpoints: (builder) => ({
    getCart: builder.query({
      query: () => 'cart',
      providesTags: ['Cart'],
    }),
    addToCart: builder.mutation({
      query: ({ id, qty }) => ({
        url: `cart/add/${id}/${qty}`,
        method: 'PATCH',
      }),
      invalidatesTags: ['Cart'],
    }),
  }),
})

export const { useGetCartQuery, useAddToCartMutation } = cartApi
