import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { getBaseUrl } from '../utils/getBaseUrl'

const baseURL = getBaseUrl()

export const userApi = createApi({
  reducerPath: 'userApi',
  baseQuery: fetchBaseQuery({ baseUrl: baseURL }),
  tagTypes: ['User'],
  endpoints: (builder) => ({
    getUsers: builder.query({
      query: () => 'user',
    }),
    loginUser: builder.query({
      query: () => '/login',
    }),
    createUser: builder.mutation({
      query: (user) => ({
        url: 'user/add',
        method: 'POST',
        body: user,
      }),
    }),
  }),
})

export const { useGetUsersQuery, useLoginUserQuery, useCreateUserMutation } =
  userApi
