import { configureStore } from '@reduxjs/toolkit/'
import { userApi } from '../services/userService'
import { cartApi } from '../services/cartService'
import $state from '../services/$state'

export default configureStore({
  reducer: {
    [userApi.reducerPath]: userApi.reducer,
    [cartApi.reducerPath]: cartApi.reducer,
    $state,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware()
      .concat(userApi.middleware)
      .concat(cartApi.middleware),
})
