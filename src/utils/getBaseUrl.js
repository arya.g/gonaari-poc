import { constants } from '../constants'

export const getBaseUrl = () => {
  let baseURL =
    process.env.NODE_ENV === 'development'
      ? 'http://localhost:5000'
      : '<our-live-endpoint>'

  if (constants.usePostmanUrl) {
    baseURL = constants.postmanUrl
  }

  return baseURL
}
